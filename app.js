const http = require('http');
const url = require('url');
const fs = require('fs');
const qs = require('querystring');
const database = require('./database/database-service');

const hostname = 'localhost';
const restApiPort = 3000;
const webServerPort = 8080;

function returnJSONResponse(res, statusCode, messageObject) {
    res.statusCode = statusCode;
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(messageObject));
}

function makeResponseObject(data, links, error) {
    var responseObject = {
        success: true,
        data: data
    };
    if (error) {
        responseObject.success = false;
        responseObject.error = error
    }
    if (links) {
        responseObject.links = links;
    }
    return responseObject;
}

function clientsGetRequest(urlParts, res) {
    database.getAllClients(qs.parse(urlParts.query), function (data, err) {
        var responseObject = {};
        var statusCode = 400;
        if (err) {
            //TODO: diversify error status codes
            statusCode = 404;
            responseObject = makeResponseObject(undefined, undefined, {
                statusCode: statusCode,
                message: 'Not Found',
                description: err.message
            });
        } else {
            statusCode = 200;
            responseObject = makeResponseObject(data);
        }
        returnJSONResponse(res, statusCode, responseObject);
    });
}

function clientsPostRequest(req, res) {
    var body = '';
    var responseObject = {};
    var statusCode = 400;
    req.on('data', function (data) {
        body += data;
        if (body.length > 1e6) {
            req.connection.destroy();
            statusCode = 413;
            responseObject = makeResponseObject(undefined, undefined, {
                statusCode: statusCode,
                message: 'Payload Too Large',
                description: ' Payload was too large, no data was saved'
            });
            returnJSONResponse(res, statusCode, responseObject);
            body = undefined;
        }
    });
    req.on('end', function () {
        if (!body) {
            return;
        }
        try {
            var requestData = qs.parse(body);
            statusCode = 400;
            database.saveClientInfo(requestData, function (client, err) {
                if (err) {
                    //TODO: diversify status codes?
                    responseObject = makeResponseObject(undefined, undefined, {
                        statusCode: statusCode,
                        message: 'Bad Request',
                        description: err.message
                    });
                } else {
                    statusCode = 201;
                    responseObject = makeResponseObject(client, {
                        details: '/client/' + client.id
                    });
                }
                returnJSONResponse(res, statusCode, responseObject)
            });
        } catch (e) {
            responseObject = makeResponseObject(undefined, undefined, {
                statusCode: statusCode,
                message: 'Bad Request',
                description: e.message
            });
            returnJSONResponse(res, statusCode, responseObject);
        }
    });
}

function clientsDetailsGetRequest(res, id) {
    database.getClientInfo(id, function (client, err) {
        var statusCode = 400;
        var responseObject = {};
        if (err) {
            statusCode = 404;
            responseObject = makeResponseObject(undefined, undefined, {
                statusCode: statusCode,
                message: 'Not Found',
                description: err.message
            });
        } else {
            statusCode = 200;
            responseObject = makeResponseObject(client);
        }
        returnJSONResponse(res, statusCode, responseObject);
    });
}

var restApi = http.createServer(function (req, res) {
    var statusCode = 406;
    var responseObject = makeResponseObject(undefined, undefined, {
        statusCode: statusCode,
        message: 'Not Acceptable',
        description: 'Method used is not supported'
    });
    var urlParts = url.parse(req.url);
    if (/^\/clients\/?$/.test(urlParts.pathname)) {
        switch (req.method) {
            case 'GET':
                clientsGetRequest(urlParts, res);
                break;
            case 'POST':
                clientsPostRequest(req, res);
                break;
            default:
                returnJSONResponse(res, statusCode, responseObject);
        }
    } else if (/^\/clients\/[0-9]+\/?$/.test(urlParts.pathname)) {
        var id = urlParts.pathname.match(/[0-9]+/);
        switch (req.method) {
            case 'GET':
                clientsDetailsGetRequest(res, id);
                break;
            default:
                returnJSONResponse(res, statusCode, responseObject);
        }
    } else {
        statusCode = 404;
        responseObject = makeResponseObject(undefined, undefined, {
            statusCode: statusCode,
            message: 'Not Found',
            description: 'Resource not found'
        });
        returnJSONResponse(res, statusCode, responseObject);
    }
}).listen(restApiPort, hostname, function () {
    console.log('REST Api server running at http://' + hostname + ':' + restApiPort + '/');
});

var webServer = http.createServer(function (req, res) {
    var urlParts = url.parse(req.url);
    if (urlParts.pathname === '/') {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'text/html');
        fs.createReadStream('./public/html/index.html').pipe(res);
    } else if (/^\/client\/add\/?$/.test(urlParts.pathname)) {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'text/html');
        fs.createReadStream('./public/html/client/add.html').pipe(res);
    } else if (/^\/css\/main.css\/?$/.test(urlParts.pathname)) {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'text/css');
        fs.createReadStream('./public/css/main.css').pipe(res);
    } else if (/^\/javascript\/index.js\/?$/.test(urlParts.pathname)) {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/javascript');
        fs.createReadStream('./public/javascript/index.js').pipe(res);
    } else if (/^\/javascript\/client\/add.js$/.test(urlParts.pathname)) {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/javascript');
        fs.createReadStream('./public/javascript/client/add.js').pipe(res);
    } else {
        res.statusCode = 404;
        res.setHeader('Content-Type', 'text/html');
        fs.createReadStream('./public/html/error.html').pipe(res);
    }
}).listen(webServerPort, hostname, function () {
    console.log('server running at http://' + hostname + ':' + webServerPort + '/')
});
