const crypto = require('crypto');

const passphrase = 'gbesfesfGWSE >w3w <3feswa"#"F3f32qa fea';
const encryptionOutputEncoding = 'hex';
const encryptionInputEncoding = 'utf8';
const algorithm = 'aes-128-cbc';

const iterations = 50;
const keylen = 16;
const digest = 'sha256';

function getSalt(salt) {
    return salt || crypto.randomBytes(32);
}
function generatePasswordAndSalt(salt, callback) {
    salt = getSalt(salt);
    crypto.pbkdf2(passphrase, salt, iterations, keylen, digest, function (err, derivedKey) {
        if (err) {
            callback(err);
            return;
        }
        var keyAndSalt = {
            key: derivedKey,
            salt: salt
        };
        callback(undefined, keyAndSalt);
    });
}

function generatePasswordAndSaltSync(salt) {
    salt = getSalt(salt);
    var derivedKey = crypto.pbkdf2Sync(passphrase, salt, iterations, keylen, digest);
    return {
        key: derivedKey,
        salt: salt
    };
}

module.exports = {
    encrypt: function(plainData, callback) {
        var iv = crypto.randomBytes(16);
        var encryptedData;
        generatePasswordAndSalt(undefined, function (err, keyAndSalt) {
            if (err) {
                callback(err);
                return;
            }
            var cipher = crypto.createCipheriv(algorithm, keyAndSalt.key, iv);
            encryptedData = cipher.update(plainData, encryptionInputEncoding, encryptionOutputEncoding);
            encryptedData += cipher.final(encryptionOutputEncoding);
            callback(undefined, keyAndSalt.salt.toString(encryptionOutputEncoding) + ':' + encryptedData + ':' + iv.toString(encryptionOutputEncoding));
        });
    },

    decrypt: function(encryptedData, callback) {
        //TODO: remove asynchronous call?
        var encryptedDataParts = encryptedData.split(':');
        if (encryptedDataParts.length !== 3) {
            callback(new Error('Encrypted data must have 3 parts salt:text:initialization vector'));
            return;
        }
        var decryptionInputEncoding = encryptionOutputEncoding;
        var decryptionOutputEncoding = encryptionInputEncoding;
        var salt = new Buffer(encryptedDataParts[0], decryptionInputEncoding);
        var encryptedText = encryptedDataParts[1];
        var iv = new Buffer(encryptedDataParts[2], decryptionInputEncoding);
        generatePasswordAndSalt(salt, function (err, keyAndSalt) {
            if (err) {
                callback(err);
                return;
            }
            var decifer = crypto.createDecipheriv(algorithm, keyAndSalt.key, iv);
            var plainText = decifer.update(encryptedText, decryptionInputEncoding, decryptionOutputEncoding);
            plainText += decifer.final(decryptionOutputEncoding);
            callback(undefined, plainText);
        });
    },
    decryptSync: function(encryptedData) {
        var encryptedDataParts = encryptedData.split(':');
        if (encryptedDataParts.length !== 3) {
            throw new Error('Encrypted data must have 3 parts salt:text:initialization vector');
        }
        var decryptionInputEncoding = encryptionOutputEncoding;
        var decryptionOutputEncoding = encryptionInputEncoding;
        var salt = new Buffer(encryptedDataParts[0], decryptionInputEncoding);
        var encryptedText = encryptedDataParts[1];
        var iv = new Buffer(encryptedDataParts[2], decryptionInputEncoding);
        var keyAndSalt = generatePasswordAndSaltSync(salt);
        var decifer = crypto.createDecipheriv(algorithm, keyAndSalt.key, iv);
        var plainText = decifer.update(encryptedText, decryptionInputEncoding, decryptionOutputEncoding);
        plainText += decifer.final(decryptionOutputEncoding);
        return plainText;
    }
};
