const mysql = require('mysql');
const phoneNumberFormat = require('google-libphonenumber').PhoneNumberFormat;
const phoneUtil = require('google-libphonenumber').PhoneNumberUtil.getInstance();
const dataCrypto = require('../data-crypto');

const phoneNumberRegion = 'GB';

const connection = mysql.createConnection({
    host: 'localhost',
    user: 'myjarTask',
    password: 'siy4ke@b23SFEyt#3G4€ESt@h34AFWF2',
    database: 'myjartask'
});
connection.connect(function (err) {
    if (err) {
        console.log('Unable to connect to database with error ' + err.message);
    } else {
        console.log('Connected to database');
    }
});

function createSQL(filter) {
    var query = {
        sql: 'SELECT DISTINCT id, email, phone FROM `Client` LEFT JOIN Client_info ON id=client_id',
        sqlVariables: []
    };
    for (var filterKey in filter) {
        if (filterKey === 'orderBy') {
            continue;
        }
        if (query.sqlVariables.length === 0) {
            query.sql += ' WHERE';
        }
        if (query.sqlVariables.length > 1) {
            query.sql += ' AND';
        }
        var filterValue = filter[filterKey];
        if (filterValue instanceof Array) {
            if (isRequiredField(filterValue)) {
                query.sql += ' ' + filterKey + ' IN (?)';
                query.sqlVariables.push(filterValue);
            } else {
                query.sql += ' (field_name LIKE ? AND field_value IN (?))';
                query.sqlVariables.push(filterKey);
                query.sqlVariables.push(filterValue);
            }
        } else {
            if (filterKey === 'id' || filterKey === 'email' || filterKey === 'phone') {
                query.sql += ' ' + filterKey + ' LIKE ?';
                query.sqlVariables.push(filterValue);
            } else {
                query.sql += ' (field_name LIKE ? AND field_value LIKE ?)';
                query.sqlVariables.push(filterKey);
                query.sqlVariables.push(filterValue);
            }
        }
    }
    if (filter.orderBy) {
        query.sql += ' ORDER BY ?';
        query.sqlVariables.push(filter.orderBy);
    }
    return query;
}

function isDatabaseResultEmpty(databaseResult) {
    return !databaseResult || databaseResult.length === 0;
}

function createClientsDataObjectFrom(databaseResult) {
    if (isDatabaseResultEmpty(databaseResult)) {
        throw new Error('No results');
    }
    var data = [];
    for (var i = 0; i < databaseResult.length; i++) {
        var entry = databaseResult[i];
        var decryptedPhone = dataCrypto.decryptSync(entry.phone);
        var displayPhone = partiallyHidePhoneNumber(decryptedPhone);
        data.push({
            id: entry.id,
            email: entry.email,
            phone: displayPhone,
            details: '/clients/' + entry.id
        });
    }
    return data;
}

function addArbitraryFieldsToDataObject(dataObject, databaseResult) {
    for (var i = 0; i < databaseResult.length; i++) {
        var entry = databaseResult[i];
        var dataKey = entry.field_name;
        if (!dataKey || !entry.field_value) {
            continue;
        }
        dataObject[dataKey] = entry.field_value;
    }
}

function partiallyHidePhoneNumber(phone) {
    phone = phone.replace(/\s+/g, '');
    return '*'.repeat(phone.length - 4) + phone.substring(phone.length - 4);
}

function createDetailedClientDataObjectFrom(databaseResult, callback) {
    if (isDatabaseResultEmpty(databaseResult)) {
        callback(new Error('No results'));
        return;
    }
    dataCrypto.decrypt(databaseResult[0].phone, function (err, decryptedPhone) {
        if (err) {
            callback(err);
            return;
        }
        var displayPhoneNumber = partiallyHidePhoneNumber(decryptedPhone);
        var clientData = {
            id: databaseResult[0].id,
            email: databaseResult[0].email,
            phone: displayPhoneNumber
        };
        addArbitraryFieldsToDataObject(clientData, databaseResult);
        callback(undefined, clientData);
    });
}

function getArbitraryFields(data, id) {
    var arbitraryFields = [];
    for (var fieldName in data) {
        if (isRequiredField(fieldName)) {
            continue;
        }
        arbitraryFields.push([id, fieldName, data[fieldName]]);
    }
    return arbitraryFields;
}

function isRequiredField(field) {
    return field === 'email' || field === 'phone' || field === 'id';
}

function insertArbitraryFieldsToDatabase(arbitraryFields, callback, data) {
    var sql = 'INSERT INTO Client_info (client_id, field_name, field_value) VALUES ?';
    connection.query(sql, [arbitraryFields], function (err, result) {
        if (err) {
            connection.rollback();
            callback(undefined, err);
            return;
        }
        connection.commit(function (err) {
            if (err) {
                callback(undefined, err);
                return;
            }
            callback(data);
        });
    });
}

function insertDataIntoDatabase(data, phone, callback) {
    connection.query('INSERT INTO `Client` (email, phone) VALUES (?, ?)', [data.email, phone], function (err, result) {
        if (err) {
            connection.rollback();
            callback(undefined, err);
            return;
        }
        var id = result.insertId;
        var arbitraryFields = getArbitraryFields(data, id);
        data.id = id;
        data.phone = partiallyHidePhoneNumber(data.phone);
        if (arbitraryFields.length === 0) {
            connection.commit(function (err) {
                if (err) {
                    callback(undefined, err);
                    return;
                }
                callback(data);
            });
        } else {
            insertArbitraryFieldsToDatabase(arbitraryFields, callback, data);
        }
    });
}

function isValidEmail(email) {
    return /^[\w!#$%&'*+-/=?^_`{|}~.]+@([\w-]+\.)+[\w]{2,}\.?$/.test(email);
}

module.exports = {
    getAllClients: function (filter, callback) {
        var query = createSQL(filter);
        connection.query(query.sql, query.sqlVariables, function (err, result) {
            if (err) {
                callback(undefined, err);
                return;
            }
            try {
                var data = createClientsDataObjectFrom(result);
                callback(data)
            } catch (e) {
                callback(undefined, e);
            }
        })
    },
    getClientInfo: function(id, callback) {
        var sql = 'SELECT id, email, phone, field_name, field_value FROM `Client` LEFT JOIN Client_info ON id=client_id WHERE id=?';
        connection.query(sql, [id], function (err, result) {
            if (err) {
                callback(undefined, err);
                return;
            }
            createDetailedClientDataObjectFrom(result, function (err, clientData) {
                if (err) {
                    callback(undefined, err);
                    return;
                }
                callback(clientData);
            });
        });
    },
    saveClientInfo: function(data, callback) {
        try {
            if (!data.phone || !data.email) {
                callback(undefined, new Error('Client info must contain a phone number and an email fields'));
                return;
            }
            var phoneNumber = phoneUtil.parse(data.phone, phoneNumberRegion);
            if (!phoneUtil.isValidNumberForRegion(phoneNumber, phoneNumberRegion)) {
                callback(undefined, new Error('Phone number must be a valid UK number'));
                return;
            }
            if (!isValidEmail(data.email)) {
                callback(undefined, new Error('Email must be valid'));
                return;
            }
        } catch (e) {
            callback(undefined, e);
            return;
        }
        connection.beginTransaction(function (err) {
            if (err) {
                connection.rollback();
                callback(undefined, err);
                return;
            }
            var phone = phoneUtil.format(phoneNumber, phoneNumberFormat.NATIONAL);
            dataCrypto.encrypt(phone, function (err, encryptedPhone) {
                if (err) {
                    callback(undefined, err);
                    return;
                }
                insertDataIntoDatabase(data, encryptedPhone, callback);
            });
        });
    }
};