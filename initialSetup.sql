CREATE DATABASE IF NOT EXISTS myjartask;

CREATE TABLE IF NOT EXISTS myjartask.`Client` (
    id BIGINT AUTO_INCREMENT,
    email VARCHAR(254),
    phone VARCHAR(162),
    CONSTRAINT PK_Client_id PRIMARY KEY (id),
    CONSTRAINT AK_Client_email UNIQUE (email)
);

CREATE TABLE IF NOT EXISTS myjartask.Client_info (
    client_id BIGINT,
    field_name VARCHAR(255),
    field_value TEXT,
    CONSTRAINT FK_Client_Client_info FOREIGN KEY (client_id) REFERENCES myjartask.`Client` (id) ON DELETE CASCADE,
    CONSTRAINT AK_Client_info_client_id_field_name UNIQUE (client_id, field_name)
);
