# README #

**Prerequisites**

NodeJS installed (developed on version 6.11.0) https://nodejs.org/en/

MySQL service running (developed on version 5.7.18.1 community server) https://dev.mysql.com/downloads/

MySQL has a user myjarTask with password siy4ke@b23SFEyt#3G4€ESt@h34AFWF2

Username and password can be changed in database-service.js file located in PROJECT ROOT/database/

initialSetup.sql located in project root has been run using mysql.

**Running server**

Running app.js (with command “node app.js”) located in project root will start two servers on ports 3000 and 8080. Default host is localhost.
They can be changed in the file.

Server running on port 3000 is for API requests.

Server running on port 8080 is for frontend, mostly for an easy way to make a post request using a html form.

More details in documentation file in /doc/ folder.