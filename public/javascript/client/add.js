var id = 0;

function addField() {
    var formList = document.getElementById('clientAddFormList');
    formList.innerHTML = formList.innerHTML +
        '<li><input type="text" onkeyup="updateTextName(' + id + ', this)"><input type="text" id="' + id + '"></li>';
    id++;
}

function updateTextName(id, caller) {
    document.getElementById(id).setAttribute('name', caller.value);
}
